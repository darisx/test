
/* custom javascripts */

		jQuery('ul.sf-menu').superfish();
	
	
	
	  // Create the dropdown base
      $("<select class='form-control mini-menu visible-xs' onchange='location = this.value;' />").appendTo("header .container");
      
      // Create default option "Go to..."
      $("<option />", {
         "selected": "selected",
         "value"   : "",
         "text"    : "Go to..."
      }).appendTo("header .container select");
      
      // Populate dropdown with menu items
      $("nav a").each(function() {
       var el = $(this);
       $("<option />", {
           "value"   : el.attr("href"),
           "text"    : el.text()
       }).appendTo("header .container select");
      });
      
	/*    // To make dropdown actually work
	   // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
      $("nav").change(function() {
        window.location = $(this).find("option:selected").val();
      });
 */
